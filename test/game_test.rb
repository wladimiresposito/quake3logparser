require_relative '../test/test_helper'
require_relative '../lib/module/means_of_death'
require_relative '../lib/model/game'
require_relative '../lib/model/player'
require_relative '../lib/model/kill'
require "minitest"

class TestGame < Minitest::Test
  def setup
    @game = Game.new
  end

  def test_add_player
    assert_equal 0, @game.players.size
    player1 = Player.new("Player Name 1")
    @game.add_player(player1)
    assert_equal 1, @game.players.size

    @game.add_player(player1)
    assert_equal 1, @game.players.size

    player1.name = "Player Name 2"
    @game.add_player(player1)
    player = @game.players.first
    assert_equal "Player Name 2", player[1].name
  end

  def test_player_is_included
    player1 = Player.new("Player Name 1")
    player2 = Player.new("Player Name 2")
    @game.add_player(player1)
    assert @game.players[player1.name] != nil
    assert_nil @game.players[player2.name]
  end

  def test_get_player_by_name
    name = "Player Name 1"
    player1 = @game.players[name]
    assert_nil player1
    player1 = Player.new(name)
    @game.add_player(player1)
    assert_equal player1, @game.players[name]
  end

  def test_add_kill
    killer = Player.new("Killer Name 1")
    killed = Player.new("Killed Name 2")
    @game.add_player(killer)
    @game.add_player(killed)
    kill = Kill.new(killer.name, killed.name, MEANS_OF_DEATH[2])
    @game.add_kill kill
    assert_equal 1, @game.total_kills
    assert_equal 1, killer.score
    assert_equal 0, killed.score
  end
end
