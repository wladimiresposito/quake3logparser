require_relative '../test/test_helper'
require_relative '../lib/model/game'
require_relative '../lib/model/player'
require_relative '../lib/model/kill'
require "minitest"

class TestPlayer < Minitest::Test
  def setup
    @game = Game.new
    @player1 = Player.new("Player1")
    @player2 = Player.new("Player2")
    @game.add_player(@player1)
    @game.add_player(@player2)
    @mod = MEANS_OF_DEATH[2]
  end

  def test_if_killed1_was_killed_by_killer1
    kill = Kill.new @player1.name, @player2.name, @mod
    @game.add_kill kill
    assert_equal 0, @player2.score
    assert_equal 1, @player1.score
  end

  def test_when_player_is_killed_by_world
    kill1 = Kill.new @player1.name, @player2.name, @mod
    kill2 = Kill.new @player1.name, @player2.name, @mod
    kill3 = Kill.new @player1.name, @player2.name, @mod
    kill_w = Kill.new "<world>", @player1.name, @mod
    @game.add_kill kill1
    @game.add_kill kill2
    @game.add_kill kill3 # now killer1 has score 3
    assert_equal 3, @player1.score
    assert_equal 0, @player2.score
    @game.add_kill kill_w # now killer1 has score 2
    assert_equal 2, @player1.score
    assert_equal 0, @player2.score
  end

  def test_when_player_is_killed_by_another_player
    kill1 = Kill.new @player1.name, @player2.name, @mod
    kill2 = Kill.new @player1.name, @player2.name, @mod
    kill3 = Kill.new @player1.name, @player2.name, @mod
    @game.add_kill kill1
    @game.add_kill kill2
    @game.add_kill kill3 # now killer1 has score 3
    assert_equal 3, @player1.score

    player3 = Player.new("Player3")
    @game.add_player player3
    kill_p = Kill.new player3.name, @player1.name, @mod
    @game.add_kill kill_p
    assert_equal 2, @player1.score
  end

  def test_when_a_player_is_killed_by_yourself
    kill1 = Kill.new @player1.name, @player2.name, @mod
    kill2 = Kill.new @player1.name, @player2.name, @mod
    kill3 = Kill.new @player1.name, @player2.name, @mod
    @game.add_kill kill1
    @game.add_kill kill2
    @game.add_kill kill3 # now killer1 has score 3
    assert_equal 3, @player1.score

    kill_yourself = Kill.new @player1.name, @player1.name, @mod
    @game.add_kill kill_yourself
    assert_equal 2, @player1.score
  end

end
