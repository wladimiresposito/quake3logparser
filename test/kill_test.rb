require_relative '../test/test_helper'
require_relative '../lib/module/means_of_death'

class TestKill < Minitest::Test
  def setup
    @mod = MEANS_OF_DEATH[2]  # MOD_GAUNTLET
    @kill = Kill.new "TheKiller", "TheKilled", @mod
  end

  def test_the_killer
    assert_equal  "TheKiller",  @kill.killer
  end

  def test_the_killed
    assert_equal  "TheKilled",  @kill.killed
  end

  def test_the_means_of_death
    assert_equal  "MOD_GAUNTLET",  @kill.kill_reason
  end
end
