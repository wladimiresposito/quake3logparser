require_relative '../test/test_helper'
require_relative '../lib/parser/quake_log_file'

class TestParser < Minitest::Test
  def setup
    @data_path = "#{File.dirname(__FILE__)}/data"
  end

  def test_when_file_does_not_exist
    log_file = QuakeLogFile.new '/this/file/does/not/exist'
    assert_equal false, log_file.open_file
  end

  def test_when_file_is_empty
    file_path = "#{@data_path}/empty_file"
    log_file = QuakeLogFile.new file_path
    log_file.open_file
    assert_equal true, log_file.is_open?
    assert_nil log_file.next_line, "File is empty"
  end

  def test_when_file_is_not_empty
    file_path = "#{@data_path}/init_game"
    log_file = QuakeLogFile.new file_path
    log_file.open_file
    assert_equal true, log_file.is_open?
    assert log_file.next_line != nil, "File is not empty"
  end
end
