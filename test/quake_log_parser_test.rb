require_relative '../test/test_helper'
require_relative '../lib/parser/quake_log_parser'
require_relative '../lib/parser/quake_log_file'
require_relative '../lib/module/parsing_rules'
require_relative '../lib/event/events'

class TestQuakeLogParser < Minitest::Test
  def setup
    @data_path = "#{File.dirname(__FILE__)}/data"
  end

  def get_log_file(file_name)
    log_path = "#{@data_path}/#{file_name}"
    log_file = QuakeLogFile.new log_path
    log_file.open_file
    return log_file
  end

  def test_if_the_game_was_initialized
    log_file = get_log_file "init_game"
    current_line = log_file.next_line
    event = QuakeLogParser.create_start_game_event current_line
    assert_equal EventType::START_GAME, event.type
    assert_equal "20:37", event.start_time
    assert event.type != EventType::END_GAME
  end

  def test_extract_player_info
    log_file = get_log_file "player_info"
    current_line = log_file.next_line
    event = QuakeLogParser.create_player_connect_event current_line
    assert_equal EventType::CONNECT, event.type
    assert_equal "Isgalamido", event.name
  end

  def test_if_has_kill_info
    log_file = get_log_file "kill_info"
    current_line = log_file.next_line
    event = QuakeLogParser.create_kill_event current_line
    assert_equal EventType::KILL, event.type
  end

  def test_extract_kill_info
    log_file = get_log_file "kill_info"
    current_line = log_file.next_line
    event = QuakeLogParser.create_kill_event current_line

    assert_equal EventType::KILL, event.type
    assert_equal "Isgalamido", event.killer
    assert_equal "Mocinha", event.killed
    assert_equal "MOD_ROCKET", event.kill_reason
  end

  def test_if_the_game_ended
    log_file = get_log_file "shutdown_game"
    current_line = log_file.next_line
    event = QuakeLogParser.create_end_game_event current_line
    assert_equal EventType::END_GAME, event.type
  end

  def test_extract_shutdown_info
    log_file = get_log_file "shutdown_game"
    current_line = log_file.next_line
    event = QuakeLogParser.create_end_game_event current_line
    assert_equal "1:47", event.end_time
  end
end
