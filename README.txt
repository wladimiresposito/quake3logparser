
The solution
============
I tried to separate the IO part from the business part
The basic idea of ​​the data structure is DDD
A log file contains several game logs
A game record contains several users
The game log reports the types of death showing who died and who killed and the reason
Each user has a performance score

Design
======
One file has several games: Game
A game has several players: Player
A game has several match records: Kill
A user has a score: int

QuakeLogFile class: it is an abstraction of a log file itself

QuakeLogParser class: is the class that parses the file and captures the game events

GamesManager: generate the model of data model and controls all the flow of parsing and production of reports

The classes of the event module represent the events during the game

The ParsingRules module concentrates all the regex rules for analyzing the log file lines

For unit tests the Gem Minitest was used.

NOTE
====

In the analysis and accounting rules it was not clear that when a player dies he has his score reduced or not.
I chosed to decrease, looking like this:
killed by <world>: decrement by one
suicide: decrease by one
player2 death by player1: player1 increases score and player2 decreases score

