require_relative "../parser/quake_log_file"
require_relative "../parser/quake_log_parser"
require_relative "../model/game"
require_relative "../model/player"
require_relative "../model/kill"

class GamesManager
  attr_accessor :games

  def initialize
    @games = { }
  end

  #
  # the another part of magic stays here
  #
  def load_games (file_path)
    quake_log_file = QuakeLogFile.new file_path
    unless quake_log_file.open_file
      abort("Cannot open the log file: #{file_path}")
    end
    quake_log_wrapper = QuakeLogParser.new quake_log_file
    event  = quake_log_wrapper.next_event
    current_game = nil
    while event != nil
      event_start = nil
      event_end = nil
      case event.type
      when EventType::START_GAME
        event_start = event
        current_game = Game.new
        current_game.start_time = event_start.start_time
      when EventType::END_GAME
        event_end = event
        if current_game == nil
          current_game = Game.new
          current_game.end_time = event_end.end_time
        else
          current_game.end_time = event_end.end_time
        end
        @games[current_game.name] = current_game unless @games[current_game.name]
        event_start = nil
        event_end = nil
      when EventType::KILL
        kill = Kill.new event.killer, event.killed, event.kill_reason
        current_game.add_kill kill
      when EventType::CONNECT
        player = Player.new event.name
        current_game.add_player player
      end
      event  = quake_log_wrapper.next_event
    end
  end

  def print_deaths_by_death_cause
    @games.each do |name, game|
      puts "\"#{name}\": {"
      puts "\t kills_by_means: {"
      game.kills_by_reason.each do |kill_array|
        puts "\t\t\"#{kill_array.first}\": #{kill_array.last},"
      end
      puts "\t}"
      puts "}\n\n"
    end
  end

  def consolidate_players
    players_hash = {}
    @games.each do |game_name, game|  # for each game
      game.players.each do |player_name, player| # for each player
        if players_hash[player_name].nil? # not added yet
          players_hash[player_name] = (Player.new player.name, player.score)
        else # ok, it was added before
          inserted_player = players_hash[player_name]
          inserted_player.score += player.score # update the score
        end
      end
    end
    #
    # Now let some magic and get all players sorted by score
    #
    players_array = players_hash.sort_by{ |k, v| v.score}.reverse!
    players_array.map {|player_array| player_array[1] }
  end

  def games_to_array
    game_array = []
    @games.each { |name, game| game_array.append game }
    return game_array
  end

end
