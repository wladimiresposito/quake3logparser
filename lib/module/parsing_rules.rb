=begin
   Al the regex parsing stuff
=end
module ParsingRules

  #game regex parsing rules
  
  def self.begin_game_rule
    /InitGame/
  end
  
  def self.end_game_rule
    /--------+/
  end
  
  def self.time_rule
    /(\d)?\d:\d\d/
  end
  
  
  #kill regex parsing rules
  
  def self.kill_rule
     /:\s([^:]+)\skilled\s(.*?)\sby\s[a-zA-Z_]+/
  end

  #kill line with the kill_rule
  def self.killer_rule
     /(?<=:\s)(.*?)(?=\skilled)/
  end

  #kill line with the killed_rule
  def self.killed_rule
     /(?<=killed\s)(.*?)(?=\sby)/
  end

  #death_reason_rule, used after the killed_rule
  def self.death_reason_rule
     /(?<=by\s)(.*?)(?=$)/
  end
  

  
  #player regex parsing rules

  # user get in rule
  def self.player_connect_rule
      /ClientUserinfoChanged: \d n\\(.*?)\\/
  end

  # to get the player name, after player_connect_rule
  def self.player_name_rule
      /(?<=\\)(.*?)(?=\\)/
  end
  
  #inquire methods for the rules
  
  def self.begin_game_line?(log_line)
    log_line =~ ParsingRules.begin_game_rule
  end
  
  def self.end_game_line?(log_line)
    log_line =~ ParsingRules.end_game_rule
  end
  
  def self.kill_line?(log_line)
    log_line =~ ParsingRules.kill_rule
  end
  
  def self.player_connect_line?(log_line)
    log_line =~ ParsingRules.player_connect_rule
  end

end
