require "json"
require_relative "module/games_manager"
#parsing log

file_name = nil
ARGV.each { |argument| file_name = argument }

begin
  file_name = "data/qgames.log" if file_name.nil?
  games_manager = GamesManager.new
  games_manager.load_games(file_name)
rescue
  puts "No file with name '#{file_name}' was found"
  exit
end

#printing output in console
puts "\n\n"
puts "==================  A QUAKE 3 GAME LOG PARSER  =================="
puts "\n\n"

puts "==================      TASK 1 + TASK 2        =================="
puts "\n\n"

puts "GENERAL RANKING OF PLAYERS (ordered by score):"
puts "\n"

ranked_players = games_manager.consolidate_players
puts JSON.pretty_generate(ranked_players)

puts "\n\n"
puts "GAMES (by log order):"
puts "\n\n"

games_array = games_manager.games_to_array
puts games_array

puts "\n\n\n"
puts "==================      TASK PLUS           =================="
puts "\n\n\n"

puts "KILLS BY GAME REPORT (ordered by deaths):\n\n"

games_manager.print_deaths_by_death_cause



