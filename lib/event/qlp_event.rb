=begin
  this is a abstraction of all events that occur in game log
  the class names explain their meaning
=end
class QlpEvent
  attr_reader :type

  public

  def initialize(type = EventType::NOTHING)
    @type = type
  end
end

class StartGameEvent < QlpEvent
  attr_reader :start_time

  public

  def initialize(start_time)
    super EventType::START_GAME
    @start_time = start_time
  end
end

class PlayerConnectEvent < QlpEvent
  attr_reader :name

  public

  def initialize(name)
    super EventType::CONNECT
    @name = name
  end
end

class EndGameEvent < QlpEvent
  attr_reader :end_time

  public

  def initialize(end_time)
    super EventType::END_GAME
    @end_time = end_time
  end
end

class KillGameEvent < QlpEvent
  attr_reader :killer, :killed, :kill_reason

  public

  def initialize(killer, killed, kill_reason)
    super EventType::KILL
    @killer = killer
    @killed = killed
    @kill_reason = kill_reason
  end
end

