=begin
  The Player class is a model like in DDD
  There is no business logic
=end
class Player
  attr_accessor :name, :score

  public
  def initialize(name, score = 0)
    @name = name
    @score = score
  end

  def increment_score
    @score += 1
  end

  def decrement_score
      @score -= 1 if @score > 0
  end

  def to_s
    "#{@name}: #{@score}"
  end

end
