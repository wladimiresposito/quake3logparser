require "json"
=begin
  The Game class is a model like in DDD
  There is no business logic
=end
class Game
  attr_reader :name, :id, :players, :kills
  attr_accessor :start_time, :end_time

  @@game_id = 1

  public
  def initialize
    @players = {}
    @kills = []
    @id = @@game_id
    @name = "game_#{@@game_id}"
    @start_time = nil
    @end_time = nil
    @@game_id += 1
  end

  #
  # Does not make sense change the name of player
  #
  def add_player(new_player)
    @players[new_player.name] = new_player unless @players[new_player.name]
  end

  def add_kill(kill)
    @kills.append kill
    update_player kill
  end

  def total_kills
    self.kills.count
  end

  #
  # The game nows how to get the kills classified byr reason of death
  #
  def kills_by_reason
    kills_by_reason_hash = {}
    self.kills.each do |kill|
      kills_by_reason_hash[kill.kill_reason] = kills_by_reason_hash[kill.kill_reason].to_i + 1
    end
    kills_by_reason_hash.sort_by{ |k, v| v}.reverse!
  end

  def to_s
    "#{@name} : #{JSON.pretty_generate(self.to_hash)}"
  end

  def to_hash
    {
        :start_time => self.start_time,
        :end_time => self.end_time,
        :total_kills => self.total_kills,
        :players => self.players_by_score.map {|player| player.name},
        :kills => self.players_by_score
    }
  end

  def players_by_score
    players_array = @players.sort_by{ |k, v| v.score}.reverse!
    players_array.map {|player_array| player_array[1] }
  end

  private
  #
  # does i have to decrement the score of a player when he is killed?
  # i dont know: i assume no
  #
  # does is have do decrement the score of a player when he suicide
  # i dont know: i assume yes
  #
  def update_player(kill)
    #hey, dont forget the world rule and suicides (by myself), of course
    if kill.killer == kill.killed || kill.killer == "<world>"
      @players[kill.killed].decrement_score #<world> kill
    else
      @players[kill.killer].increment_score #normal kill
      @players[kill.killed].decrement_score #normal kill
    end
  end
end
