=begin
  The Kill class is a model like in DDD
  There is no business logic
=end
class Kill
  attr_reader :killer, :killed, :kill_reason

  def initialize(killer, killed, kill_reason)
    @killer = killer
    @killed = killed
    @kill_reason = kill_reason
  end
end
