=begin
  QuakeLogFile abstracts the log file and io aspects
  This is the io stuff
=end
class QuakeLogFile
  attr_reader :filePath
  attr_writer :file, :is_open

  public

  def initialize(file_path = "data/quake.log")
    @file_path = file_path
    @file = nil
    @is_open = false
  end

  def open_file
    begin
      @file = File.new(@file_path)
      @is_open = true
    rescue Exception => msg
      puts "File not Found, please correct it!"
      puts msg
      @is_open = false
    end
    return @is_open
  end

  def is_open?
    @is_open
  end

  def close
    @file.close unless @file == nil
  end

  def reset
    close
    open_file
  end

  def next_line
    if is_open?
      line = @file.gets
      if line == nil
        @file.close
        @is_open = false
      end
      return line
    end
    return nil
  end
end
