require_relative "../event/events"
require_relative "../event/qlp_event"
require_relative "../module/parsing_rules"

=begin
  QuakeLogParser is a wrapper over the log file hidding the io aspects
  is uses a state machine (next_event) to process the lines from log file
=end
class QuakeLogParser
  attr_writer :current_state
  attr_reader :quake_log_file, :count_lines, :current_line
  @@ST_BEGIN_FILE = 0
  @@ST_BEGIN_GAME = 1
  @@ST_END_GAME = 2
  @@ST_BEGIN_GAME_NO_END = 3
  @@ST_MISSING_END_GAME = 4
  @@ST_PLAYER_CONNECT = 5
  @@ST_KILL_GAME = 6
  @@ST_END_FILE = 7
  @@ST_NOTHING = 8

  public
  def initialize(quake_log_file)
    @quake_log_file = quake_log_file
    @quake_log_file.reset
    @current_state = @@ST_BEGIN_FILE
    @count_lines = 0
    @current_line = nil
  end

  def reset
    @quake_log_file.reset
    @current_state = @@ST_BEGIN_FILE
    @count_lines = 0
    @current_line = nil
  end

=begin
  The MAGIC begins here
  next_event is controlled by a state machine according current_state
  next_event only returns when a event is found, skipping unnecessary lines
  the state names explain their meaning
  when there is no more data it returns nil
=end
  def next_event
    line = @quake_log_file.next_line
    @current_line  = line
    while line != nil  # i dont like the use of nil as false
      @count_lines += 1
      case @current_state
      when @@ST_BEGIN_FILE, @@ST_END_GAME
        if ParsingRules.begin_game_line? line
          @current_state = @@ST_BEGIN_GAME
          return QuakeLogParser.create_start_game_event line
        end
      when @@ST_BEGIN_GAME, @@ST_KILL_GAME, @@ST_PLAYER_CONNECT
        if ParsingRules.kill_line? line
          @current_state = @@ST_KILL_GAME
          return QuakeLogParser.create_kill_event(line)
        elsif ParsingRules.end_game_line? line
          @current_state = @@ST_END_GAME
          return QuakeLogParser.create_end_game_event(line)
        elsif ParsingRules.begin_game_line? line # begin game event with no end game event
          @current_state = @@ST_MISSING_END_GAME
        elsif ParsingRules.player_connect_line? line # player arrives
          @current_state = @@ST_PLAYER_CONNECT
          return QuakeLogParser.create_player_connect_event(line)
        end
      when @@ST_MISSING_END_GAME
        @current_state = @@ST_BEGIN_GAME_NO_END
        return QuakeLogParser.create_end_game_event(line)
      when @@ST_BEGIN_GAME_NO_END
        @current_state = @@ST_BEGIN_GAME
        return QuakeLogParser.create_start_game_event line
      end
      line = @quake_log_file.next_line
      @current_line = line
    end
    return nil
  end

#
# I have some class methods
# I decided stay here for simplicity
#
  def self.create_player_connect_event(line)
    first_match = line.match(ParsingRules.player_connect_rule)[0]
    player_name = first_match.match(ParsingRules.player_name_rule)[0]
    connect_event = PlayerConnectEvent.new player_name
    return connect_event
  end

  def self.create_kill_event(line)
    first_match = line.match(ParsingRules.kill_rule)[0]
    killer = first_match.match(ParsingRules.killer_rule)[0]
    killed = first_match.match(ParsingRules.killed_rule)[0]
    death_reason = first_match.match(ParsingRules.death_reason_rule)[0]
    kill_event = KillGameEvent.new killer, killed, death_reason
    return kill_event
  end

  def self.create_end_game_event(line)
    end_time = line.match(ParsingRules.time_rule)[0]
    end_event = EndGameEvent.new end_time
    return end_event
  end

  def self.create_start_game_event(line)
    start_time = line.match(ParsingRules.time_rule)[0]
    start_event = StartGameEvent.new start_time
    return start_event
  end
end
